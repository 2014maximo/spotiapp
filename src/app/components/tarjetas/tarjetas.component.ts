import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styles: []
})
export class TarjetasComponent {

  @Input() items: any[] = [];

  constructor( private router: Router) { }

  // Método para linkar al artista clicado
  verArtista( item: any ) {
    let artistaId;

    // Dentro del servicio ya cargado
    // se busca el atributo "type"
    // si es artist toma ese id sino
    // lo busca en otra parte que igual se encuentra

    if ( item.type === 'artist')
    {
      artistaId = item.id;
    } else {
      artistaId = item.artists[0].id;
    }

    // a través de este proceso se dirige al link
    // artista y le lleva el id del artista
    this.router.navigate(['/artist', artistaId]);
  }

}

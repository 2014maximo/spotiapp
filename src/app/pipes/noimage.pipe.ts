import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimage' // NOMBRE DEL PIPE
})
export class NoimagePipe implements PipeTransform {

  transform( images: any[] ): string {  // ESTE MÉTODO ES EL QUE REALIZA LA TAREA REQUERIDA

    if (!images) {
      return 'assets/img/noimages.png';
    }

    if ( images.length > 0 ) {
      return images[0].url;
    } else {
      return 'assets/img/noimages.png';
    }

  }

}
